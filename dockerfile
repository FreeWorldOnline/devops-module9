FROM mcr.microsoft.com/dotnet/sdk:6.0

RUN curl -fsSL https://deb.nodesource.com/setup_19.x | bash - &&\
apt-get install -y nodejs

COPY . .
RUN dotnet build
RUN dotnet test

WORKDIR /DotnetTemplate.Web
RUN npm install && find ./node_modules/ ! -user root | xargs chown root:root
RUN npm run build

RUN npm t
RUN npm run lint

WORKDIR /DotnetTemplate.Web
ENTRYPOINT [ "dotnet", "run" ]